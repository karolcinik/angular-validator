import {AbstractControl, ValidatorFn} from "@angular/forms";

export const emailRegex = new RegExp('^(([^<>()\\[\\]\\\\.,;:\\s@"]+(\\.[^<>()\\[\\]\\\\.,;:\\s@"]+)*)' +
  '|(".+"))@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}])' +
  '|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$');

export function maxLengthValidator(maxLength: number, errorId: string): ValidatorFn {
  return (control: AbstractControl): { [key: string]: boolean } | null => {
    const isMaxLength = control.value.length < maxLength;
    if (control.value && !isMaxLength) {
      const obj = {};
      obj[errorId] = !isMaxLength;
      return obj;
    }
    return null;
  };
}

export function patternValidator(pattern: RegExp, errorId: string): ValidatorFn {
  return (control: AbstractControl): { [key: string]: boolean } | null => {
    const isMatching = pattern.test(control.value);
    if (control.value && !isMatching) {
      const obj = {};
      obj[errorId] = !isMatching;
      return obj;
    }
    return null;
  };
}
