import { Component, OnInit } from '@angular/core';
import {FormBuilder, Validators} from "@angular/forms";
import {maxLengthValidator} from "../../validator/Validators";

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {

  constructor(private formBuilder: FormBuilder) {
  }

  inputMaxLength = 10;

  form = this.formBuilder.group({
    inputFormField: ['', Validators.compose(
      [Validators.required, maxLengthValidator(this.inputMaxLength, 'inputMaxLengthError')])],
  });

  ngOnInit(): void {
  }

  isHidden() {
    return this.form.get('inputFormField').pristine
  }

  isError() {
    return this.form.get('inputFormField').getError('inputMaxLengthError')
  }

  onSubmit() {

  }
}
